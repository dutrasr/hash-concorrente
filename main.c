/* Funções a serem implementadas:
 * OK - Um tipo entrada, o qual deve ter pelo menos um número inteiro a ser armazenado (a chave);
 * OK - constructor(m,n,entrada), o qual inicializa uma tabela hash com m variáveis do tipo entrada sincronizados em n blocos(m múltiplo de n);
 * OK - destructor(), libera a tabela hash após adquirir controle de todos os blocos;
 * OK - get(x), retorna o elemento de chave x se este existir;
 * OK - add(x), adiciona um elemento de chave x se este não existir;
 * OK - set(x), adiciona um elemento de chave x se não existir, ou muda seu valor caso já exista;
 * OK - delete(x), remove o elemento de chave x se este existir;
 * OK - print(x), imprime o conteúdo do elemento de chave x ou null se este não existir;
 * OK - printall(), imprime todos os elementos da tabela;
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


    int *tabela;
    int *ocupado;
    int ocp;
    float carga;
    int tamanho;

    void constructor(int, int);
    void aumentaHash();
    void zera_tabela();
    void entrada(int);
    void destructor();
    void add(int);
    int get(int);
    void set(int);
    void delete(int);
    void print(int);
    void printall();


/*
 * 
 */
int main(int argc, char** argv) {
    
    constructor(5, 1);
    zera_tabela();
    int i;
    printall();
    add(20);
    add(10);
    add(15);
    add(20);
    add(27);
    add(25);
    add(29);
    add(3);
    add(17);
    set(10);
    print(10);
    delete(10);
    print(10);
    printall();
    destructor();
    return (EXIT_SUCCESS);
}



void constructor(int m, int n){
    tabela = (int*) malloc(m * sizeof (int));
    ocupado = (int*) malloc(m * sizeof (int));
    tamanho = m;
}

void aumentaHash(){
    printf("\nExpandindo...\n");
    int tab_aux[tamanho];
    int tab_ocup[tamanho];
    int i;
    for (i=0;i < tamanho;i++){
        tab_aux[i] = tabela[i];
        tab_ocup[i] = ocupado[i];
    }
    
    tamanho *= 2;
    free(tabela);
    free(ocupado);
    tabela = (int*) malloc(tamanho * sizeof (int));
    ocupado = (int*) malloc(tamanho * sizeof (int));
    zera_tabela();
    for(i=0;i<tamanho/2;i++){
        if(tab_ocup[i] == 1)
            entrada(tab_aux[i]);
    }
    //free(tab_aux);
    //free(tab_ocup);
}

void zera_tabela(){
    int i;
    for (i=0;i<tamanho;i++){
        tabela[i] = -1;
        ocupado[i]= 0;
    }
}

int get(int val){
    int pos = val%tamanho;
        while (ocupado[pos]==1){
            if (tabela[pos]==val){
                return pos;
            }
            else{
                pos++;
                if (pos > tamanho-1)
                    pos = 0;
            }
            
        }
        return (int)NULL;
    }

void entrada(int val){
    int pos;
        pos = val%tamanho;
        while (ocupado[pos]==1){
            pos++;
            if (pos > tamanho-1)
                pos = 0;
        }
        tabela[pos] = val;
        ocupado[pos]=1;
}

void add(int val){
    int pos;
    if(tabela[get(val)] != val){
        pos = val%tamanho;
        while (ocupado[pos]==1){
            pos++;
            if (pos > tamanho-1)
                pos = 0;
        }
        tabela[pos] = val;
        ocupado[pos]=1;
        ocp++;
        carga = (float)ocp/(float)tamanho;
        if (carga > 0.75){
            aumentaHash();
        }
    }else
        printf("Elemento ja existente na tabela!!!");
}

void set(int val){
    int pos;
    if(tabela[get(val)] != val){
        pos = val%tamanho;
        tabela[pos] = val;
        if(ocupado[pos] != 1){
            ocupado[pos]=1;
            ocp++;
        }
        carga = (float)ocp/(float)tamanho;
        if (carga > 0.75){
            aumentaHash();
        }
    }
}

void delete(int val){
     int pos = val%tamanho;
        while (ocupado[pos]==1){
            if (tabela[pos]==val){
                ocupado[pos] = 0;
            }
            else{
                pos++;
                if (pos > tamanho-1)
                    pos = 0;
            }
            
        }
}

void print(int val){
    int pos = val%tamanho;
        while (ocupado[pos]==1){
            if (tabela[pos]==val){
                printf("pos %d: %d\n", pos, tabela[pos]);
                return;
            }
            else{
                pos++;
                if (pos > tamanho-1)
                    pos = 0;
            }
        }
        return;
}

void printall(){
    int i;
    for (i=0;i<tamanho;i++){
        if(ocupado[i] == 1)
            printf("pos %d: %d\n", i, tabela[i]);
    }
}

void destructor(){
        free(tabela);
        free(ocupado);
}