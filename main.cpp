/* Funções a serem implementadas:
 * Um tipo entrada, o qual deve ter pelo menos um número inteiro a ser armazenado (a chave);
 * constructor(m,n,entrada), o qual inicializa uma tabela hash com m variáveis do tipo entrada sincronizados em n blocos(m múltiplo de n);
 * destructor(), libera a tabela hash após adquirir controle de todos os blocos;
 * OK - get(x), retorna o elemento de chave x se este existir;
 * add(x), adiciona um elemento de chave x se este não existir;
 * set(x), adiciona um elemento de chave x se não existir, ou muda seu valor caso já exista;
 * OK - delete(x), remove o elemento de chave x se este existir;
 * OK - print(x), imprime o conteúdo do elemento de chave x ou null se este não existir;
 * OK - printall(), imprime todos os elementos da tabela;
 * 
 * CHECAR A FUNÇÃO DE BUSCA QUE ANTES SE A TABELA ESIVE-SE CHEIA E O ELEMENTO A SER PROCURADO NÃO EXISTI-SE NA TABELA, 
 * A MESMA ENTRAVA EM LOOP INFINITO.
 * 
 * PROBLEMA NA FUNÇÃO AUMENTAHASH, QUE QUANDO VAI DELETAR OS VETORES AUXILIARES OCORRE ALGUM TIPO DE ERRO QUE 
 * EU NÃO CONSEGUI CONCERTAR AINDA. 
 */
#include <iostream>
#include <algorithm>

using namespace std;

class hash
{
private:
    int *vetor,tamanho,ocp;
    float carga;
    char *ocupado;

    void aumentaHash ()
    {
        cout << " ::: Aumento ::: " << endl;
        getStatus();
        cout << " ::: Tabela  :::" << endl;
        printHash();
        cout << endl;
        int aux[tamanho],i;
        char auxoc[tamanho];
        copy(&vetor[0],&vetor[tamanho],&aux[0]);
        copy(&ocupado[0],&ocupado[tamanho],&auxoc[0]);
        tamanho*=2;
        vetor = new int[tamanho];
        ocupado = new char[tamanho];
        ocp = 0;
        fill(&ocupado[0],&ocupado[tamanho],0);
        for (i=0;i<tamanho/2;i++)
            if (auxoc[i]==1)
                insValor(aux[i]);
        //PROBLEMA COM O DELETE!!!
        //delete[] aux;
        //delete[] auxoc;


    }
    
public:
    
    hash ()
    {
        tamanho = 2;
        ocp=0;
        carga=0;
        vetor = new int[tamanho];
        ocupado = new char[tamanho];
        fill(&ocupado[0],&ocupado[tamanho],0);
    }
    
    void insValor(int val)
    {
        int pos;
        pos = val%tamanho;
        while (ocupado[pos]==1)
            pos++;
        vetor[pos] = val;
        ocupado[pos]=1;
        ocp++;
        carga = (float)ocp/(float)tamanho;
        if (carga > 0.75)
            aumentaHash();

    }

    void buscaValor (int val)
    {
        int pos = val%tamanho;
        while (ocupado[pos]==1)
        {
            if (vetor[pos]==val)
            {
                cout << "Valor encontrado na posicao " << pos << endl;
                return;
            }
            else
                pos++;
                if (pos > tamanho-1)
                    pos = 0;
            }
            cout << "Valor nao encontrado." << endl;
            return;
    }
    
    void printHash ()
    {
        int i;
        for (i=0;i<tamanho;i++)
            if (ocupado[i]==1)
                cout << "pos " << i << ": " <<vetor[i] << endl;
    }
    
    void getStatus ()
    {
        cout << "Tamanho:" << tamanho << endl;
        cout << "Ocp:" << ocp << endl;
        cout << "Carga:" << carga << endl;
    }
    
    void printElementoHash(int val){
        int pos = val%tamanho;
        while (ocupado[pos]==1){
            if (vetor[pos]==val){
                cout << "pos " << pos << ": " <<vetor[pos] << endl;
                return;
            }
            else{
                pos++;
                if (pos > tamanho-1)
                    pos = 0;
            }
        }
        return;
    }
    
    void removeElementoHash(int val){
        int pos = val%tamanho;
        while (ocupado[pos]==1){
            if (vetor[pos]==val){
             ocupado[pos] = 0;
            }
            else{
                pos++;
                if (pos > tamanho-1)
                    pos = 0;
            }
        }
        cout << "Chave não encontrada na tabela!!!" << endl;
        return;
    }
    
    int getElementoHash(int val){
        int pos = val%tamanho;
        while (ocupado[pos]==1){
            if (vetor[pos]==val){
                return pos;
            }
            else{
                pos++;
                if (pos > tamanho-1)
                    pos = 0;
            }
            
        }
        return int(NULL);
    }
    
    void setElementoHash(int val){
        
    }

};

int main()
{
    hash tabela;
    tabela.insValor(10);
    tabela.insValor(20);
    tabela.buscaValor(1000);
    tabela.insValor(40);
    tabela.insValor(80);
    tabela.insValor(1000);
    tabela.buscaValor(1000);
    cout << "get:" << tabela.getElementoHash(1000) << endl;
    tabela.printElementoHash(1000);
    cout << " ::: Final   :::" << endl;
    tabela.getStatus();
    cout << " ::: Tabela  :::" << endl;
    tabela.printHash();
    tabela.removeElementoHash(1000);
    cout << "get:" << tabela.getElementoHash(1000) << endl;
    tabela.printElementoHash(1000);
    tabela.printElementoHash(20);
    cout << " ::: Final II  :::" << endl;
    tabela.getStatus();
    cout << " ::: Tabela  :::" << endl;
    tabela.printHash();
    return 0;
}
